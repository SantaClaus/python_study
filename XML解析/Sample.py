import xml.etree.ElementTree as ET
import openpyxl as PX

class Sample:
    if __name__ == '__main__':
        parsed = ET.parse('hogehoge.xml')
        root = parsed.getroot()

        # パースしたXMLをexcelに落とし込む・・・とします。
        workBook = PX.Workbook()
        activeSheet = workBook.active
        i = 2 #1行目は手動で列名を入れる・・・とします。

        # 名前空間fugaがあるXMLを想定。詳しくはopenpyxlのドキュメント見て
        for label in root.findall('{fuga}labels'):
            activeSheet['A'+str(i)].value = label.find('{fuga}key1').text
            activeSheet['B'+str(i)].value = label.find('{fuga}key2').text
            i+=1
        workBook.save('出力先+ファイル名')