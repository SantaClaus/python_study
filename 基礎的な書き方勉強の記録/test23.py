#例外処理やraise
#raise→エラー時に上位にスローすることだね

import traceback

def exception_test(num1, num2):
    sum = 0

    try:
        #例外が発生しうるコード
        sum = num1 + num2
        return sum
    except:
        #例外処理
        print('計算できないよ')
        raise
    else:
        #例外発生ないときの処理。どんなとき使うんだ？
        pass
    finally:
        #必ず最後に通る処理
        print('処理終了')

if __name__ == '__main__':
    try:
        print(exception_test(1,1,))
        print(exception_test(1,'1'))
    except:
        print('raiseされたよ')
        #どっちも同じ結果
        print(traceback.format_exc())
        #traceback.print_exc()