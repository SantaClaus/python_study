import traceback

class MyException(Exception):
    pass
def hoge():
    print('fugafuga')
    raise MyException('任意のメッセージ付けれるぞ') from ZeroDivisionError

if __name__ == '__main__':
    try:
        hoge()
    except MyException as e:
        print(e)
        print('---------------------------------------')
        print(traceback.format_exc())
        print('---------------------------------------')