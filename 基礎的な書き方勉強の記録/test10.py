#タプル(要素の追加・削除できない)
def tuple():
    value_tuple = ('A',1,[2,3],{'key':'value','key1':'value2'})

    for x in range(0,len(value_tuple)):
        print(value_tuple[x])

    print(value_tuple[2][0])
    print(value_tuple[3].get('key'))

#リスト(タプルとの違い：追加・削除可能)
def list():
    value_list = ['A',1,[2,3],{'key':'value','key1':'value2'}]

    for x in value_list:
        print(x)

    print(value_list[2][0])
    print(value_list[3].get('key1'))

    #追加
    value_list.append(100)
    value_list.append('tuika')
    print(value_list)

    url = ['http:','www.']
    #インデックス指定追加
    url.insert(1,'//')
    url.insert(3,'google.com')
    print(url)
    #要素指定削除
    url.remove('www.')
    #インデックス指定削除
    url.pop(2)
    print(url)
    url.append('http:')
    #最初に見つかった要素のインデックス
    print(url.index('http:'))
    #該当する要素の数
    print(url.count('http:'))

#ディクショナリ(≒連想配列)
def dic():
    dict = {'YEAR':'2010', 'MONTH':'1', 'DAY':'20'}

    for x in dict:
        print(x) #繰り返し処理でkeyが取れる
        print(dict[x])

    #追加
    dict['today'] = '20180612'
    print(dict.get('today'))
    #keyだけ・valueだけ・同時
    print(dict.keys())
    print(dict.values())
    print(dict.items())
    for key, value in dict.items():
        print(key, value)
    #削除
    del dict['today']
    print(dict.get('today'))
    #keyの確認
    print('YEAR' in dict)

#セット(重複した値をもてないリスト的な)
#frozesetという更新できないセットもあるよ
def setto():
    #セットの中にリストとディクショナリ持てない・・・？(unhashableエラー)
    sett = {'A',1}

    for x in sett:
        print(x)

    #ディクショナリとして扱われる
    dict = {}
    #セットとして扱われる
    settt = {'A'}
    setttt = set()

    #追加
    test_set_1 = set()
    test_set_1.add('python')
    test_set_1.update({'-', 'izm', '.', 'com'})
    print(test_set_1)
    #削除
    test_set_1.remove('-')
    test_set_1.discard('.')
    print(test_set_1)

if __name__ == '__main__':
    #tuple()
    #list()
    #dic()
    setto()