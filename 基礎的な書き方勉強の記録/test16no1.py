import test16

test_class_1 = test16.TestClass()
test_class_1.test_method('1')

#外部モジュールの関数や変数もfrom importで利用できる。
from test16 import TestClass

test_class_2 = TestClass()
test_class_2.test_method('2')

#別名でインポート
from test16 import TestClass as aaa

test_class_3 = aaa()
test_class_3.test_method('3')