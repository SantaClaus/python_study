#継承・親クラスメソッド利用


class oya:
    def __init__(self,name,kind):
        self.name = name
        self.kind = kind
    def show(self):
        print(self.name)
        print(self.kind)

class ko(oya):
    def __init__(self,name,kind,hoge):
        super().__init__(name,kind)
        self.hoge = hoge
    def show(self):
        super().show()
        print(self.hoge)

if __name__ == '__main__':
    koclass = ko('takashi','neko','fuga')
    koclass.show()