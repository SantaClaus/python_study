#print文(2系は取り扱わない)

#基本
print('test1')

#文末の値を決めるend引数は省略すると\nが与えられている。だから改行されるんだね
print('test2',end='')
print('',end='\n')
#出力先の変更
#ファイル引数を付けると、printの出力先をコンソールからファイルに変えるってことかな
f_obj = open('fileopentest.txt', 'w')
print('testdayo', file = f_obj)

#フォーマット(古いやり方はCとかと同じ。format引数は新しい)
strdayo = 'STR'
intdayo = 123
print('mozi:{0} , suuzi:{1}'.format(strdayo,intdayo))