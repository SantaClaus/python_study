import traceback
from test26 import MyException

def output_traceback(self):
        print('---------------------------------------')
        print(traceback.format_exc())
        print('---------------------------------------')

def myfunc():
    try:
        #[][0]          #IndexError
        #raise MyException('自作例外発生！')  #自作例外
        num = 1 / 0         #ZeroDivisionError
    except MyException as e:
        MyException.my_funcA('AAA')
        MyException.my_funcB(1,2,3)
        output_traceback(e)
        raise MyException({'message':'mine'}) from e #発生源のraiseの引数上書きしてる
    except (ZeroDivisionError,IndexError) as original:
        output_traceback(original)
        raise MyException(*original.args) from original #MyExceptionにオリジナルの情報を付けてraise
    finally:
        print('end')

if __name__ == '__main__':
    try:
        myfunc()
    except MyException as e:
        print('MyException = {}'.format(e))
        #print(e.args)
        output_traceback(e)