import traceback

def exception_test(num1, num2):
    sum = 0
    try:
        sum = num1 + num2
        return sum
    except TypeError as e:
        print('例外内容 = [{}]'.format(e))
        raise
    finally:
        print('処理終了')

if __name__ == '__main__':
    try:
        print(exception_test(1,'1'))
    except (ZeroDivisionError,TypeError):
        print('a')
    except TypeError:
        print('-------------------------------')
        print(traceback.format_exc())
        print('-------------------------------')