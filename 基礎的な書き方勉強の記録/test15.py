ans = 'Hello'
ans += ' '

#標準入力から入力される行数が未定
#まず入力行を把握
N = int(input())
#その回数だけ入れる
s = [input() for i in range(N)]

counter = 0
for x in s:
    ans += x
    if counter != (len(s) - 1):
        ans += ','
    else:
        ans += '.'
    counter += 1
print(ans)