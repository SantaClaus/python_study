#rangeとxrange
print(range(10))

list1 = [range(10)]
print(list1)

for i in range(-5,5):
    print(i)

#xrangeとは？
#2系のときに使うやつだよ。3系だとあんまり関係ないのかな
#3系で下記のようにxrange使うとエラーになるよ
for i in xrange(10):
    print(i)