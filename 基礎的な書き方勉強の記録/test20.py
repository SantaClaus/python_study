#条件文

value = 2
value2 = 1
if value == 1 or value2 == 2:
    print('value = {0}'.format(value))
elif value == 2 and value2 == 1:
    print('value = {0} , value2 = {1}'.format(value,value2))
else:
    #passは「何もしない」を意味する。インデントのブロック構造で空行を持てないため
    pass

list1 = [1,2,'c','d',5]
for x in list1:
    print(x)
for x in 'ABCDE123':
    print(x)
dict1 = {'A':'B', 'C':'D', 'E':'F', 'F':'G'}
for x, y in dict1.items():
    print('\'{0}\':\'{1}\''.format(x,y))

counter = 0
while counter < 10:
    counter += 1
    print(counter,end='')
counter = 0
while True:
    counter += 1
    print(counter,end='')
    if counter >= 10:
        break

for num in range(100):
    if num % 10:
        continue
    print(num)