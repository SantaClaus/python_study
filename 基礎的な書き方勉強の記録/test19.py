#パス結合・連結 os.path.joinを使う。osをインポート
import os

PROJECT_DIR = 'C:\python\past'
SETTING_FILE = 'settings.ini'

print(os.path.join(PROJECT_DIR, SETTING_FILE))
print(os.path.join(PROJECT_DIR, 'setting_dir', SETTING_FILE))
#環境によってファイルセパレータ（円マーク）変わる