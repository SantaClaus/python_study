def print_test():
	print ('シングルクォーテーション')
	print ("ダブルクォーテーション")
	test_str = """
				test_1
				test_2
				"""
	print (test_str)

	test_str = 'python'
	test_str = test_str + '-'
	test_str = test_str + 'takashi'
	print (test_str)

	test_str = '012'
	test_str += '345'
	test_str += '678'
	test_str += '910'
	print (test_str)

	test_str = '012' * 4
	print (test_str)

	test_integer = 100
	print (str(test_integer) + '円')

	test_str = 'tikansuruyo'
	print ((test_str).replace('tikan' , 'chikan'))

	test_str = 'ika/tako/neko/tora/kitune/kuzira/hogei'
	print ((test_str).split('/'))

	test_str = '1234'
	print ((test_str).rjust(10,'0'))
	print ((test_str).rjust(10,'!'))

	test_str = '1234'
	print ((test_str).zfill(10))
	print ((test_str).zfill(3))

	test_str = 'hello_world'
	print ((test_str).startswith('hello'))
	print ((test_str).startswith('world'))

	test_str = 'hello_world'
	print ('o' in test_str)
	print ('s' in test_str)

	test_str = 'Python-Izm.com'
	print ((test_str).upper())
	print ((test_str).lower())

	print ('----------------------------------')
	test_str = '     python-izm.com'
	print (test_str)

	test_str = test_str.lstrip()
	print (test_str)

	test_str = test_str.lstrip('python')
	print (test_str)
	print ('----------------------------------')

	test_str = 'python-izm.com     '
	print ((test_str) + '/')

	test_str = test_str.rstrip()
	print ((test_str) + '/')

	test_str = test_str.rstrip("com")
	print (test_str)


if __name__ == '__main__':
	print_test()