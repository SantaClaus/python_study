#可変長引数ってなんだよ

def sugoine(*args):
    print(args)

#仮引数に*があると可変長引数になるんだね
sugoine(1,2,3,4,5,6,7,8,)
sugoine({'n':'a'},[1,2,3,4,5])
#関数にはタプルとして渡されるそう。


def sugoi(a,b,*args):
    print(a,b)
    print(args)
sugoi(100,'uhouho','GOLIRA','KUMA')
#もちろんargsという名前は慣習だぞ

#下記でタプルがディクショナリになるぞ
def dict(**kwargs):
    print(kwargs)
dict(key='a',value='b')
#ディクショナリとして渡せない値はエラーはくので、注意