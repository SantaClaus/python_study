#継承  pythonは多重継承もできるけど処理呼出順がヒデるから慎重に適切に

class Country:
    def __init__(self,name):
        self.name = name

#↓これで継承。多重継承はカンマで区切ってかく。
class City(Country):
    def __init__(self,name,city):
        super().__init__(name)
        self.city = city

classes = []
classes.append(City('Japan', 'Tokyo'))
classes.append(City('USA', 'Washington, D.C.'))

for test_cls in classes:
    print('===== Class =====')
    print('country_name --> ' + test_cls.name)
    print('city_name --> ' + test_cls.city)