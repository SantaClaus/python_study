import re
import subprocess as SP

class Sample:
    if __name__ == '__main__':
        SP.run('ここにコマンド', cwd='実行するディレクトリ')

        # shell=Trueを付けるかはコマンドによる。詳しくはsubprocessのドキュメント見て
        SP.run('ここにコマンド', cwd='実行するディレクトリ', shell=True)

        # コマンド実行後にターミナルに表示されるアウトプットを何かに利用したい場合
        output = SP.run('ここにコマンド', cwd='実行するディレクトリ', shell=True, stdout=SP.PIPE)
        str = re.findall('ある引数から抽出したい文字列に対する正規表現', output.stdout.decode('utf-8'))[0] # 特定の引数から抽出して欲しい場合はargument[n]を指定
        str2 = output.stdout.decode('utf-8') # アウトプットがまるまる欲しい場合
        str3 = output.stdout.decode('utf-8')[2] # （動かしてないけど）特定の引数まるまる欲しい場合はこうかな？