class Sample:
    if __name__ == '__main__':
        # 複数行あるファイルを読み込む
        with open('ファイルパス', 'r') as fr:
            detalines = fr.readlines()
            for index, line in enumrate(detalines):
                if line == '書き換えたい行': detalines = "書き換えた後の値"
        # 書き込む
        with open('ファイルパス', 'w') as fw:
            for index, line in enumrate(detalines):
                fw.write(detalines[index])
